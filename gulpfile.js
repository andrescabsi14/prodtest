'use strict';

var srcEndCss = 'styles.min.css'; //Concatenated CSS
var srcSass = 'src/scss/**/*.scss';// Styles source
var destCss = 'src/css'; // Compiled CSS destination folder
var destCssProd = 'public/css'; // Compiled CSS destination folder in production
var srcImg = 'src/images/**/*'; // Images source
var destImg = 'public/images'; // Optimized images destination folder

// Gulp
var gulp = require('gulp');

// Plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var imagemin = require('gulp-imagemin');

// SASS TASK
gulp.task('sass', function() {
    return gulp.src(srcSass)
    	.pipe(sourcemaps.init())
		.pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer(
            {
                browsers: ['last 2 version'],
                cascade: false
            }
        ))
	    .pipe(concat(srcEndCss))
	    .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(destCss))
        .pipe(notify({
    		title: "SASS Compiled",
    		message: 'SASS compiled successfully <%= file.relative %>' 
    	}));
});

// MINIFY CSS TASK
gulp.task('minify-css', function() {
    return gulp.src(srcSass)
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(concat(srcEndCss))
        .pipe(cleanCSS().on('error', function(err) {
	        return notify({
	    		title: "Clean CSS Error",
	    	}).write(err);
	    }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(destCssProd))
        .pipe(notify({ 
        	title: "CSS Minified",
        	message: 'CSS Minified <%= file.relative %>' 
        }));
});

//IMAGES OPTIMIZATION
gulp.task('images', function() {
    return gulp.src(srcImg)
        .pipe(imagemin().on('error', function(err) {
            return notify({
                title: "Image optimization task error",
            }).write(err);
        }))
        .pipe(gulp.dest(destImg))
        .pipe(notify({
            title: "Images optimized",
            message: 'Optimized image <%= file.relative %>' 
        }));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(srcImg, ['images']);
    gulp.watch(srcSass, ['sass']);
});

// Default Task
gulp.task('default', ['sass', 'watch']);

//Production 
gulp.task('production', ['minify-css', 'images']);
