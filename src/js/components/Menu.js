'use strict'

//Dependencies
import React from 'react';

//Components
import Buttons from './MenuButtons';

export default class Menu extends React.Component{
	constructor(){
		super();
	}
	render(){

		return (
			<article className="menu_widget">
				<ul>
					{
						this.props.data.map( (button) => {
							return <Buttons
								key={button.id}
								icon={button.icon}
								title={button.title}
								notification={button.notification} />
						})				
					}
				</ul>
				<div class="main_title">{this.props.author}</div>
			</article>
		)
	}
}




