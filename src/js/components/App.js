'use strict'

//Dependencies
import React from 'react';

//Components
import Header from './Header';
import Footer from './Footer';
import Cards from './Cards';
import User from './User';
import Forex from './Forex';

export default class Layout extends React.Component{
	constructor (props) {
		super(props)
		this.state = {
			menudata: [],
			userMenudata: [],
			postsdata: []
		}
	}

	loadMenu () {
	    fetch("data/menu.json")
			.then(response => response.json())
			.then(data => this.setState({ menudata: data.mainmenu.list, author: data.mainmenu.author }))
			.catch(err => console.error(this.props.url, err.toString()))
	}
	loadUserMenu () {
	    fetch("data/menu.json")
			.then(response => response.json())
			.then(data => this.setState({ usermenudata: data.usermenu.buttons }))
			.catch(err => console.error(this.props.url, err.toString()))
	}
	loadPosts () {
	    fetch("data/posts.json")
	      .then(response => response.json())
	      .then(data => this.setState({ postsdata: data.list }))
	      .catch(err => console.error(this.props.url, err.toString()))
	}

	componentDidMount () {
		this.loadMenu()
		this.loadUserMenu()
		this.loadPosts()
	}



	render(){
		return (
			<div className="main_wrapper">
				<Header menus={this.state.menudata} author={this.state.author} />
				<section className="widgets_container">
					<Cards posts={this.state.postsdata} />
					<div class="sidebar_wrapper">
						<User datalist={this.state.usermenudata} />
						<Forex />
					</div>
				</section>
				<Footer />
			</div>
		)
	}
}