'use strict'

//Dependencies
import React from 'react';


export default class Card extends React.Component{
	constructor(){
		super();
	}
	render(){
		
		return (
			<article className="card_widget">
				<div className="image_wrapper">
					<img src={this.props.cover} alt="Profile Cover" />
				</div>
				<div className="info_wrapper">
					<div className="profile_image">
						<div className="image_container">
							<img src={this.props.avatar} alt="Profile Image" />
						</div>
					</div>
					<div className="profile_info">
						<h1>{this.props.title}</h1>
						<div className="content" dangerouslySetInnerHTML={{__html: this.props.description}}>
						</div>
					</div>
				</div>
				<div className="bottom_stats">
					<div className="views">
						<div className="icon">
							<img src={'images/views_count.png'} alt="Views" />
						</div>
						<div className="views_count count">{this.props.viewsc}</div>
					</div>
					<div className={this.props.commentsc >= 1 ? 'comments' : 'comments hidden'}>
						<div className="icon">
							<img src={'images/comments_count.png'} alt="Comments" />
						</div>
						<div className="comments_count count">{this.props.commentsc}</div>
					</div>
					<div className={this.props.likesc >= 1 ? 'likes' : 'likes hidden'}>
						<div className="icon">
							<img src={'images/like_count.png'} alt="Likes" />
						</div>
						<div className="likes_count count">{this.props.likesc}</div>
					</div>
				</div>
			</article>
		)
	}
}