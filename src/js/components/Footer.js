import React from 'react';

export default class Footer extends React.Component{
	constructor(){
		super();
		this.website = "acdeveloper.com";
		this.link = "http://www.acdeveloper.com"
	}
	render(){
		return (
			<footer>
				<div className="website">
					<a href="{this.link}" target="_blank">{this.website}</a>
				</div>
				<ul className="languages">
					<li className="english">
						<img src="./images/colombia.png" alt="" />
					</li>
					<li className="spanish">
						<img src="./images/english.png" alt="" />
					</li>
				</ul>
			</footer>
		)
	}
}