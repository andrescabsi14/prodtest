import React from 'react';


export default class Forex extends React.Component{
	constructor(){
		super();
	}
	render(){		
		return (
			<article className="forex_widget">
				<div className="stats_wrapper">
					<div id="chart" className="chart"></div>
					<div className="summary_wrapper">
						<div className="open_val">$3.000</div>
						<div className="close_val">$3.500</div>
					</div>
				</div>
				<div className="forex_wrapper">
					<div className="location_wrapper">
						<div className="location">Salt Lake City, Utah</div>
						<div className="title">USD vs COP</div>
					</div>
					<div className="forex_summary">
						<div className="open">
							<div className="open_value">$3.100</div>
							<div className="description">Open Value</div>
						</div>
						<div className="close">
							<div className="close_value">$3.200</div>
							<div className="description">Close Value</div>
						</div>
					</div>			
				</div>
				<div className="bottom_stats">
					<div className="average_wrapper">
						<div className="title">AVERAGE</div>
						<div id="stats_average" className="stats_average"></div>
					</div>

					<div className="yearly_change_wrapper">
						<div className="title">Yearly Change</div>
						<div className="yearly_change">+127.07</div>
					</div>
				</div>
			</article>
		)
	}
}