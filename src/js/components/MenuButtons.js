'use strict'

//Dependencies
import React from 'react';

export default class Buttons extends React.Component{
	constructor(){
		super();
	}
	render(){
		return (
			<li class="active">
				<div className="icon">
					<span className={this.props.icon}></span>
				</div>
				<div className="title">
					{this.props.title}
					<div className={this.props.notification.length >= 1 ? 'notification' : 'notification hidden'}>
						{this.props.notification.length}
					</div>
				</div>
			</li>
		)
	}
}




