'use strict'

//Dependencies
import React from 'react';


export default class UserMenu extends React.Component{
	constructor(){
		super();
	}
	render(){
		return (
			<li>
				<div className="title">Edit user</div>
				<div className="icon">
					<img className="normal" src={'images/user_icon.png'} alt="Edit User" />
					<img className="hover" src={'images/user_icon_h.png'} alt="Edit User" />
				</div>
			</li>
		)
	}
}




