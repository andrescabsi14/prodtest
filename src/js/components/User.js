'use strict'

//Dependencies
import React from 'react';

//Components
import UserMenu from './UserMenu';

export default class User extends React.Component{
	constructor(){
		super();
	}
	render(){

		return (
			<article className="user_widget">
				<div className="user_wrapper">
					<div className="profile_image">
						<div className="image_container">
							<img src={'images/user_avatar.jpg'} alt="Profile Image" />
						</div>
					</div>
					<div className="profile_info">
						<h1>COURTNEY TIMMONS</h1>
						<div className="followers">15,323 followers</div>
					</div>
				</div>
				<div className="user_menu_wrapper">
					<ul>
						<li>
							<div className="title">Edit user</div>
							<div className="icon">
								<img className="normal" src={'images/user_icon.png'} alt="Edit User" />
								<img className="hover" src={'images/user_icon_h.png'} alt="Edit User" />
							</div>
						</li>
						<li>
							<div className="title">Web statistics</div>
							<div className="icon">
								<img className="normal" src={'images/sound_icon.png'} alt="Web statistics" />
								<img className="hover" src={'images/sound_icon_h.png'} alt="Web statistics" />
							</div>
						</li>
						<li>
							<div className="title">Upload settings</div>
							<div className="icon">
								<img className="normal" src={'images/spanner_icon.png'} alt="Upload settings" />
								<img className="hover" src={'images/spanner_icon_h.png'} alt="Upload settings" />
							</div>
						</li>
						<li>
							<div className="title">Events</div>
							<div className="icon">
								<img className="normal" src={'images/pin_map_icon.png'} alt="Events" />
								<img className="hover" src={'images/pin_map_icon_h.png'} alt="Events" />
							</div>
						</li>


						{/*
							this.props.datalist.map( (button) => {
								return <UserMenu key={button.id} icon={button.icon} iconHover={button.iconHover} title={button.title} />
							})				
						*/}
						
					</ul>
				</div>
			</article>
		)
	}
}




