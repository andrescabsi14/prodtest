'use strict'

//Dependencies
import React from 'react';
import $ from 'jquery';

//Components
import Card from './Card';

export default class Cards extends React.Component{

	render(){
		return (
			<div class="user_posts_wrapper">
				{
					this.props.posts.map( (post) => {
						return <Card
							key={post.id}
							title={post.title}
							cover={post.cover}
							avatar={post.avatar}
							description={post.description}
							viewsc={post.views}
							commentsc={post.comments}
							likesc={post.likes}  />
					})
					
				}
			</div>
		)
	}
}