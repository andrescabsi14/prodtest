'use strict'

//Dependencies
import React from 'react';

//Components
import Menu from './Menu';

export default class Header extends React.Component{
	constructor(){
		super();
	}
	render(){
		return (
			<header>
				<Menu author={this.props.author} data={this.props.menus} />
			</header>
		)
	}
}